<?php
// This file is part of the Xpert URL download repository plugin for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * A repository plugin to allow user downloading of image from a URL and appending copyright attribution
 * 
 * @package    repository_xpert_url
 * @copyright  2013 University of Nottingham
 * @author     Barry Oosthuizen <barry.oosthuizen@nottingham.ac.uk>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
require_once($CFG->dirroot . '/repository/lib.php');

/**
 * A subclass of repository, which is used to download a file from a specific url
 *
 * @package    repository_xpert_url
 * @copyright  2013 University of Nottingham
 * @author     Barry Oosthuizen <barry.oosthuizen@nottingham.ac.uk>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class repository_xpert_url extends repository {

    /**
     * Class constructor
     * 
     * @param int $repositoryid
     * @param object $context
     * @param array $options
     */
    public function __construct($repositoryid, $context = SYSCONTEXTID, $options = array()) {
        global $CFG;
        parent::__construct($repositoryid, $context, $options);

        $this->file_url = optional_param('file', '', PARAM_RAW);
    }

    /**
     * To check whether the user is logged in.
     *
     * @return boolean
     */
    public function check_login() {
        if (!empty($this->file_url)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Get image size options
     *
     * @return array $sizes
     */
    public static function get_sizes() {

        $sizes = array();
        $sizes['standard'] = get_string('standard', 'repository_xpert_url');
        $sizes['small'] = get_string('small', 'repository_xpert_url');
        $sizes['medium'] = get_string('medium', 'repository_xpert_url');
        $sizes['large'] = get_string('large', 'repository_xpert_url');

        return $sizes;
    }

    /**
     * Create the initial form for the filepicker
     * @return mixed
     */
    public function print_login() {
        $strdownload = get_string('download', 'repository');
        $strurl      = get_string('url', 'repository_xpert_url');
        if ($this->options['ajax']) {

            $url = new stdClass();
            $url->label = $strurl.': ';
            $url->id   = 'fileurl';
            $url->type = 'text';
            $url->name = 'file';

            $author = new stdClass();
            $author->label = get_string('author', 'repository_xpert_url') .': ';
            $author->id   = 'author';
            $author->type = 'text';
            $author->name = 'author';

            $license = new stdClass();
            $license->type = 'select';
            $license->options = array(
                (object)array(
                    'value' => 'public',
                    'label' => get_string('public', 'repository_xpert_url'),
                    'selected' => 'selected'
                ),
                (object)array(
                    'value' => 'cc',
                    'label' => get_string('cc', 'repository_xpert_url')
                ),
                (object)array(
                    'value' => 'cc-nd',
                    'label' => get_string('cc-nd', 'repository_xpert_url')
                ),
                (object)array(
                    'value' => 'cc-nc-nd',
                    'label' => get_string('cc-nc-nd', 'repository_xpert_url')
                ),
                (object)array(
                    'value' => 'cc-nc',
                    'label' => get_string('cc-nc', 'repository_xpert_url')
                ),
                (object)array(
                    'value' => 'cc-nc-sa',
                    'label' => get_string('cc-nc-sa', 'repository_xpert_url')
                ),
                (object)array(
                    'value' => 'cc-sa',
                    'label' => get_string('cc-sa', 'repository_xpert_url')
                ),
                (object)array(
                    'value' => 'unknown',
                    'label' => get_string('unknown', 'repository_xpert_url')
                ),
                (object)array(
                    'value' => 'allrightsreserved',
                    'label' => get_string('allrightsreserved', 'repository_xpert_url')
                )
            );
            $license->id = 'license';
            $license->name = 'license';
            $license->label = get_string('license', 'repository_xpert_url') . ': ';

            $size = new stdClass();
            $size->type = 'select';
            $size->options = array(
                (object)array(
                    'value' => 'standard',
                    'label' => get_string('standard', 'repository_xpert_url')
                ),
                (object)array(
                    'value' => 'small',
                    'label' => get_string('small', 'repository_xpert_url')
                ),
                (object)array(
                    'value' => 'medium',
                    'label' => get_string('medium', 'repository_xpert_url'),
                    'selected' => 'selected'
                ),
                (object)array(
                    'value' => 'large',
                    'label' => get_string('large', 'repository_xpert_url')
                )
            );
            $size->id = 'size';
            $size->name = 'size';
            $size->label = get_string('size', 'repository_xpert_url') . ': ';

            $copyrightyear = new stdClass();
            $copyrightyear->label = get_string('copyrightyear', 'repository_xpert_url') .': ';
            $copyrightyear->id   = 'year';
            $copyrightyear->type = 'text';
            $copyrightyear->name = 'year';

            $colours = new stdClass();
            $colours->label = get_string('colour', 'repository_xpert_url') . ': ';
            $colours->type = 'radio';
            $colours->id = 'colours';
            $colours->name = 'colours';
            $colours->value = 'colourwhiteblack|colourblackwhite|colourgreyblack|colourbluebrown|colourpeachbrown|colouryellowblack|colourpinkblack';
            $colours->value_label = '<div class="colourwhiteblack">'
                    . get_string('colourwhiteblack', 'repository_xpert_url') . '</div>|' .
                    '<div class="colourblackwhite">'
                    . get_string('colourblackwhite', 'repository_xpert_url') . '</div>|' .
                    '<div class="colourgreyblack">'
                    . get_string('colourgreyblack', 'repository_xpert_url') . '</div>|' .
                    '<div class="colourbluebrown">'
                    . get_string('colourbluebrown', 'repository_xpert_url') . '</div>|' .
                    '<div class="colourpeachbrown">'
                    . get_string('colourpeachbrown', 'repository_xpert_url') . '</div>|' .
                    '<div class="colouryellowblack">'
                    . get_string('colouryellowblack', 'repository_xpert_url') . '</div>|' .
                    '<div class="colourpinkblack">'
                    . get_string('colourpinkblack', 'repository_xpert_url') . '</div>';

            $ret['login'] = array($url, $author, $license, $size, $copyrightyear, $colours);
            $ret['login_btn_label'] = get_string('download', 'repository_xpert_url');
            $ret['allowcaching'] = true; // Indicates that login form can be cached in filepicker.js.

            return $ret;

        } else {

            $sizes = self::get_sizes();

            $template = '<table>';
            // Text element for URL.
            $template .= '<tr class="fp-url">';
            $template .= '<td class="mdl-right"><label>' . $strurl . '</label>:</td>';
            $template .= '<td class="mdl-left"><input type="text" name="url"/></td></tr>';
            // Select element for size.
            $template .= '<tr class="size"><td class="mdl-right"><label>' . get_string('size') . '</label>:</td>';
            $template .= '<td class="mdl-left"><select id="size" name="size">';
            foreach ($sizes as $sizekey => $sizename) {
                $template     .= '<option value="' . $sizekey . '">' . $sizename . '</option>';
            }
            $template .= '</select></td></tr>';
            // Text element for copyright year.
            $template .= '<tr class="fp-year">';
            $template .= '<td class="mdl-right"><label>' . get_string('copyrightyear', 'repository_xpert_url') . '</label>:</td>';
            $template .= '<td class="mdl-left"><input type="text" name="year"/></td></tr>';
            // Background and text colour options.
            $template .= '<td class="mdl-right"><label>' . get_string('colour', 'repository_xpert_url') . '</label>:</td>';
            $template .= '<td class="mdl-left">';
            $template .= '<div class="colourradiobutton"><input type="radio" name="colour" value="colourwhiteblack" checked><div class="colourwhiteblack">';
            $template .= get_string('colourwhiteblack', 'repository_xpert_url'). '</div></div><br/>';
            $template .= '<div class="colourradiobutton"><input type="radio" name="colour" value="colourblackwhite"><div class="colourblackwhite">';
            $template .= get_string('colourblackwhite', 'repository_xpert_url') . '</div></div><br/>';
            $template .= '<div class="colourradiobutton"><input type="radio" name="colour" value="colourgreyblack"><div class="colourgreyblack">';
            $template .= get_string('colourgreyblack', 'repository_xpert_url') . '</div></div><br/>';
            $template .= '<div class="colourradiobutton"><input type="radio" name="colour" value="colourbluebrown"><div class="colourbluebrown">';
            $template .= get_string('colourbluebrown', 'repository_xpert_url') . '</div></div><br/>';
            $template .= '<div class="colourradiobutton"><input type="radio" name="colour" value="colourpeachbrown"><div class="colourpeachbrown">';
            $template .= get_string('colourpeachbrown', 'repository_xpert_url') . '</div></div><br/>';
            $template .= '<div class="colourradiobutton"><input type="radio" name="colour value="colouryellowblack"><div class="colouryellowblack">';
            $template .= get_string('colouryellowblack', 'repository_xpert_url') . '</div></div><br/>';
            $template .= '<div class="colourradiobutton"><input type="radio" name="colour" value="colourpinkblack"><div class="colourpinkblack">';
            $template .= get_string('colourpinkblack', 'repository_xpert_url') . '</div></div><br/>';
            $template .= '</td></tr>';

            $template .= '</table><input type="submit" value="' . $strdownload . '" />';
            echo $template;

        }
    }

    /**
     * Get main repository page.
     * @param mixed $path
     * @param int $page
     * @return boolean
     * @throws moodle_exception
     */
    public function get_listing($path='', $page='') {
        global $SESSION;

        $ret = array();
        $ret['list'] = array();
        $ret['nosearch'] = true;
        $ret['norefresh'] = true;
        $ret['nologin'] = true;

        $author = optional_param('author', '', PARAM_TEXT);
        $license = optional_param('license', '', PARAM_TEXT);
        $size = optional_param('size', '', PARAM_TEXT);
        $year = optional_param('year', '', PARAM_INT);
        $colours = optional_param('colours', '', PARAM_TEXT);
        if ($author == '') {
            throw new moodle_exception('noauthorspecified', 'repository_xpert_url');
        }

        $url = $this->file_url;
        $this->sess_author     = 'xpert_url_'.$this->id.'_author';
        $this->sess_license     = 'xpert_url_'.$this->id.'_license';
        $this->sess_url     = 'xpert_url_'.$this->id.'_url';
        $this->sess_year    = 'xpert_url_'.$this->id.'_year';
        $this->sess_size    = 'xpert_url_'.$this->id.'_size';
        $this->sess_colours    = 'xpert_url_'.$this->id.'_colours';

        if (!empty($this->file_url)) {
            $SESSION->{$this->sess_author}  = $author;
            $SESSION->{$this->sess_license}  = $license;
            $SESSION->{$this->sess_url}  = $url;
            $SESSION->{$this->sess_year} = $year;
            $SESSION->{$this->sess_size} = $size;
            $SESSION->{$this->sess_colours} = $colours;
        }

        $this->add_image_to_list($this->file_url, $ret, $author, $license);
        return $ret;
    }

    /**
     * Get the file from the specified URL
     * @param string $source
     * @param string $file
     * @return array with elements:
     *   path: internal location of the file
     *   url: URL to the source (from parameters)
     */
    public function get_file($source, $file = '') {
        global $SESSION;

        $author = $SESSION->{'xpert_url_'.$this->id.'_author'};
        $license = $SESSION->{'xpert_url_'.$this->id.'_license'};
        $url = $SESSION->{'xpert_url_'.$this->id.'_url'};
        $year = $SESSION->{'xpert_url_'.$this->id.'_year'};
        $size = $SESSION->{'xpert_url_'.$this->id.'_size'};
        $colourcombo = $SESSION->{'xpert_url_'.$this->id.'_colours'};

        if ($size == 'standard') {
            $size = 'medium';
        }

        $path = $this->prepare_file($file);
        $fp = fopen($path, 'w');
        $c = new curl;
        $c->download(array(array('url' => $source, 'file' => $fp)));
        // Must close file handler, otherwise gd lib will fail to process it.
        fclose($fp);
        $img = new repository_xpert_url_image($path, $author, $url, $license, $size, $year, $colourcombo);
        $img->saveas($path);

        return array('path' => $path, 'author' => $author, 'license' => $license);
    }

    /**
     * Add file details to $list
     * @param string $url
     * @param array $list
     * @param string $author
     * @param int $license
     * @throws moodle_exception
     */
    protected function add_image_to_list($url, &$list, $author, $license) {

        if (!file_extension_in_typegroup($url, 'web_image')) {
            throw new moodle_exception('invalidfile');
        } else {
            if (empty($list['list'])) {
                $list['list'] = array();
            }

            $list['list'][] = array(
                'title' => $this->guess_filename($url),
                'source' => $url,
                'thumbnail' => $url,
                'thumbnail_height' => 84,
                'thumbnail_width' => 84,
                'author' => $author,
                'license' => $license
            );
        }
    }

    /**
     * Guess the filename based on the URL
     * @param string $url
     * @return string
     */
    public function guess_filename($url) {
        $pattern = '#\/([\w_\?\-.]+)$#';
        $matches = null;
        preg_match($pattern, $url, $matches);
        if (empty($matches[1])) {
            return $url;
        } else {
            return $matches[1];
        }
    }

    /**
     * Return a list of return types supported by the Xpert url downloader plugin
     * @return string
     */
    public function supported_returntypes() {
        return (FILE_INTERNAL);
    }

    /**
     * Return the source information
     *
     * @param stdClass $url
     * @return string|null
     */
    public function get_file_source_info($url) {
        return $url;
    }

    /**
     * file types supported by the Xpert url downloader plugin
     *
     * @return array
     */
    public function supported_filetypes() {
        return array('web_image');
    }

    /**
     * Is this repository accessing private data?
     *
     * @return bool
     */
    public function contains_private_data() {
        return false;
    }

    /**
     * destroy session
     *
     * @return object
     */
    public function logout() {
        global $SESSION;

        unset($SESSION->{$this->sess_url});
        unset($SESSION->{$this->sess_year});
        unset($SESSION->{$this->sess_size});
        unset($SESSION->{$this->sess_author});
        unset($SESSION->{$this->sess_license});
        unset($SESSION->{$this->sess_colours});
        return $this->print_login();
    }

    /**
     * Add Plugin settings input to Moodle form
     * @param object $mform
     * @param string $classname
     */
    public static function type_config_form($mform, $classname = 'repository') {

        parent::type_config_form($mform, $classname);

        $mform->addElement('checkbox', 'debug', get_string('debug', 'repository_xpert_url'));
        $mform->setType('debug', PARAM_INT);
        $mform->addElement('static', '', '', get_string('debug_description', 'repository_xpert_url'));
        $mform->setDefault('debug', 0);
    }

    /**
     * save debug setting in config table
     * @param array $options
     * @return boolean
     */
    public function set_option($options = array()) {

        if (!empty($options['debug'])) {
            set_config('debug', trim($options['debug']), 'xpert_url');
        }
        unset($options['debug']);
        return parent::set_option($options);
    }

    /**
     * Names of the plugin settings
     * @return array
     */
    public static function get_type_option_names() {
        return array('pluginname', 'debug');
    }
}
